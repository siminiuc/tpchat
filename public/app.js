let username;
var message = $("#message");
var connectedUsers = $(".connected-users");
var typing = false;
var timeout;
let socket = null;
let id;
let secret = null;
let unload = false;

function init(){
  socket = io('/tpchat', {
    reconnection: false
  });

  socket.on("connect", function(){

    $("#message").prop("disabled", false);

    socket.on('new user', function(e){
      $("#messages").append(generateMessage(e.text, "ADMIN"));
      socket.on("secret verified", function(e){
        secret = e.secret;
      });
    });

    socket.on('custom id', function(e){
      id = e;
    });

    socket.emit("request messages");

    socket.on("saved messages", function(e){
      var messages = e.message;
      messages.forEach(message => {
        if(message.type === "user-message"){
          $("#messages").append(generateMessage(generateStrangerMessage(message.name, message.message), "OTHER"));
        }else if(message.type === "admin-message"){
          $("#messages").append(generateMessage(message.message, "ADMIN"));
        }
      });
      scrollToBottom();
    });

    socket.on("user disconnected", function(e) {
      $("#messages").append(generateMessage(e.name + " Disconnected", "ADMIN"));
      if(document.getElementById('container-' + e.id) != null){
        setTimeout(function(){
          document.getElementById('container-' + e.id).remove();
          toggleOverflowBody();
        }, 5000);
      }
      scrollToBottom();
    });

    socket.on("request username", function(){
      socket.emit("username", {
        name: username
      });
    });

    socket.on("user connected", function(e) {
      $("#messages").append(generateMessage(e.name + " Joined", "ADMIN"));
      scrollToBottom();
    });

    socket.on("newMessage", function(msg) {
      $("#messages").append(generateMessage(generateStrangerMessage(msg.name, msg.message), "OTHER"));
      scrollToBottom();
    });

    socket.on("users", function(e){
      $("p .users").html("<b>" + e.numbers + "</b>");
    });

    socket.on("is typing", function(e){
      if(e.typing == true){
        $(".typing").text(e.user + " is typing...");
      }else{
        $(".typing").text("");
      }
    });

    socket.on("cleared", function(){
      $(".typing").text("");
    });

    socket.on("allUsers", function(e){
      $(".users-list").html("");
      e.forEach(function(e){
        if(secret != null && secret != " "){
          showAllUsers(e, true);
        }else{
          showAllUsers(e, false);
        }
      });
    });

    socket.on("request connection", function(e){
      if(!document.getElementsByClassName('modal-container').length){
        //e.to is you
        if(document.getElementById("container-" + e.to) == null){
          createMessageRequestModal(e.from, id, e.name);
        }
      }
    });

    socket.on("private message", function(e){
      //from is them
      //to is you
      $("#private-messages-" + e.from).append(generateMessage(generateStrangerMessage(e.name, e.message), "OTHER"));
      if(!$('#container-' + e.from).is(':visible')){
        $('[data-id="'+ e.from +'"]').addClass('show-notification');
        if(!$('.all-users-container').is(':visible')){
          $('.hamburger').addClass('show-notification');
        }
      }
    });

    socket.on("private image", function(e){
      $("#private-messages-" + e.from).append(generateMessage(generateImageMessage(e.data), "OTHER"));
    });

    socket.on("message status", function(e){
      setTimeout(function(){
        if(e.status === "success"){
          createChatPopup(e.name, e.to, e.from);
          $("#private-messages-" + e.to).append(generateMessage("You have been connected", "ADMIN"));
          window.location.hash = e.to;
        }
      }, 1000);
    });

    socket.on("private user disconnect", function(e){
      //from is them
      //to is you
      $("#private-messages-" + e.from).append(generateMessage(e.name + " Has Left", "ADMIN"));
      $("#private-message-" + e.from).prop("disabled", true);
    });

    $("#message").focus(function(){
      scrollToBottom();
    });

    $("#message").on("input", function(e){
      if(e.which != 13){
        typing = true;
        socket.emit("typing", {
          user: username,
          typing: typing
        });
        clearTimeout(timeout);
        timeout = setTimeout(typingTimeout, 1500);
      }else{
        clearTimeout(timeout);
        typingTimeout();
      }
    });

    $(".hamburger").click(function(e){
      $(this).removeClass('show-notification');
      $(this).toggleClass('active');
      $(".chat-form-container").animate({"bottom": "-100%"}, 500);
      setTimeout(function(){
        $(".all-users-container").show();
        $(".list").animate({"left": "0px"}, 500);
      }, 300);
    });

    $(".close-list-div").click(function(){
      closeList();
      $('.hamburger').toggleClass('active');
    });

    $(".close-users-list").click(function(e){
      closeList();
      $('.hamburger').toggleClass('active');
    });

  });

  $(".users-list").on("click", ".list-name", function(){
    var parent = $(this).parent();
    to = parent.data("id");
    if(document.getElementById("container-" + to) != null){
      parent.removeClass('show-notification');
      $("#container-" + to).fadeIn(500);
      window.location.hash = to;
      toggleOverflowBody();
    }else{
      connectUser(to, id);
    }
  });

  $(".users-list").on("click", ".remove-user", function(){
    var parent = $(this).parent();
    var tmpId = parent.data("id");
    if(tmpId != id){
      removeUser(tmpId);
    }
  });

  socket.on('disconnect', function(e){
    connectionStatus();
    window.onbeforeunload = null;
    message.prop('disabled', true);
  });

  window.onbeforeunload = function() {
    if(unload == false){
      unload = true;
      return "Are you sure you want to leave :/";
    }else{
      unload = false;
    }
  };

  window.addEventListener('hashchange', hashChanged);

  // showAds();

}