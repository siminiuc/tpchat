function init(){
    socket = io('/pixel-art', {
        reconnection: false
    });

    socket.on('connect', function(){
        id = socket.id;
        console.log(id);
    });

    socket.emit('new pixel user', {
        name: name,
        group: groupName
    });

    socket.emit('request drawing');

    socket.on('existing drawing', function(e){
        if(e != null && e.length > 0){
            e.forEach(element => {
                var cords = element.cords;
                onDrawingEvent(cords); 
            });
        }
    });

    socket.on('drawing', onDrawingEvent);
}

function updateUsername(){
    let name = $('#username-field').val();
    let groupName = $('#group-name').val();
    if(!name == '' && groupName != ''){
        $('#username-field').val('');
        $('#group-name').val('');
        hidePopupContainer();
        username = name;
        // window.history.pushState({}, '', '?username='+ name + '&groupName='+ groupName);
        window.location = '?username=' + name + '&groupName=' + groupName;
        init();
        initCanvas();
    }else{
        $('#message').html('<p class="mt-5 text-red">Please fill all fields</p>');
    }
}

function initCanvas(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    ctx = canvas.getContext('2d');
    canvas.addEventListener('mousedown', onMouseDown, false);
    canvas.addEventListener('mouseup', onMouseUp, false);
    canvas.addEventListener('mouseout', onMouseUp, false);
    canvas.addEventListener('mousemove', onMouseMove, false);
    
    //Touch support for mobile devices
    canvas.addEventListener('touchstart', onMouseDown, false);
    canvas.addEventListener('touchend', onMouseUp, false);
    canvas.addEventListener('touchcancel', onMouseUp, false);
    canvas.addEventListener('touchmove', onMouseMove, false);
}

function hidePopupContainer(){
    $(".popup-container").hide();
}

function drawLine(x0, y0, x1, y1, emit){
    ctx.beginPath();
    ctx.moveTo(x0, y0);
    ctx.lineTo(x1, y1);
    ctx.lineWidth = 2;
    ctx.stroke();
    ctx.closePath();

    if (!emit) { return; }
    var w = canvas.width;
    var h = canvas.height;
    
    socket.emit('drawing', {
        x0: x0 / w,
        y0: y0 / h,
        x1: x1 / w,
        y1: y1 / h
    });
}

function onMouseDown(e){
    if(e.target == canvas){
        window.blockMenuHeaderScroll = true;
    }
    drawing = true;
    current.x = e.clientX||e.touches[0].clientX;
    current.y = e.clientY||e.touches[0].clientY;
}

function onMouseUp(e){
    blockMenuHeaderScroll = false;
    if (!drawing) { return; }
    drawing = false;
    drawLine(current.x, current.y, e.clientX||e.touches[0].clientX, e.clientY||e.touches[0].clientY, current.color, true);
}

function onMouseMove(e){
    if(blockMenuHeaderScroll){
        e.preventDefault();
    }
    if (!drawing) { return; }
    drawLine(current.x, current.y, e.clientX||e.touches[0].clientX, e.clientY||e.touches[0].clientY, current.color, true);
    current.x = e.clientX||e.touches[0].clientX;
    current.y = e.clientY||e.touches[0].clientY;
}

function onDrawingEvent(data){
    var w = canvas.width;
    var h = canvas.height;
    drawLine(data.x0 * w, data.y0 * h, data.x1 * w, data.y1 * h);
}