let username;
let socket = null;
let id = null;
let secret = null;
let ctx;
let drawing = false;
let lastX, lastY;
var current = {
    color: 'black'
};
var canvas = $('#pixel-art')[0];
window.blockMenuHeaderScroll = false;

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const name = urlParams.get('username');
const groupName = urlParams.get('groupName');
if(name != null && groupName != null){
    hidePopupContainer();
    init();
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    ctx = canvas.getContext('2d');
    canvas.addEventListener('mousedown', onMouseDown, false);
    canvas.addEventListener('mouseup', onMouseUp, false);
    canvas.addEventListener('mouseout', onMouseUp, false);
    canvas.addEventListener('mousemove', onMouseMove, false);
    
    //Touch support for mobile devices
    canvas.addEventListener('touchstart', onMouseDown, false);
    canvas.addEventListener('touchend', onMouseUp, false);
    canvas.addEventListener('touchcancel', onMouseUp, false);
    canvas.addEventListener('touchmove', onMouseMove, false);
}

$('.username-send').on('click', updateUsername);