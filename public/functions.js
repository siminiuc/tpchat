function typingTimeout(){
  socket.emit("clear");
}

function scrollToBottom(){
  $('html').animate({
    scrollTop: $('html').height()
  }, 500);
}

function updateUsername(){
  let name = $("#username-field").val();
  let secret = $("#secret").val();
  if(name != "" && name != " " && name.length <= 38){
    init();
    $("#username-field").val("");
    $(".username-popup").hide();
    $(".popup-container").hide();
    username = name;
    socket.emit("new user", {
      name: username,
      secret: secret
    });
    $("#message").prop("disabled", false);
  }
  return false;
}

function sendMessage(){
  $(".chat-form").submit(function(e){
    e.preventDefault();
    var msg = $("#message").val().trim();
    if(msg != "" && msg != " " && msg.length <= 200){
      socket.emit("newMessage", {
        name: username,
        message: msg
      });
      $(document).scrollTop($(document).height());
      $("#messages").append(generateMessage(generateOwnMessage(msg), "YOU"));
      scrollToBottom();
    }
    $("#message").val("");
    return false;
  });
}

function showAllUsers(data, secret){
  if(secret == true){
    var li = document.createElement('li');
    var span = document.createElement('span');
    var removeUser = document.createElement('span');
    var removeUserIcon = document.createElement('i');
    li.classList.add('users');
    li.setAttribute('data-name', data.name);
    li.setAttribute('data-id', data.customID);
    span.classList.add('list-name');
    span.append(data.name);
    removeUser.classList.add('remove-user');
    removeUserIcon.classList.add('fa', 'fa-trash');
    removeUser.append(removeUserIcon);
    li.append(span);
    li.append(removeUser);
  }else if(secret == false){
    var li = document.createElement('li');
    var span = document.createElement('span');
    li.classList.add('users');
    li.setAttribute('data-name', data.name);
    li.setAttribute('data-id', data.customID);
    span.classList.add('list-name');
    span.append(data.name);
    li.append(span);
  }
  document.getElementById('users-list').append(li);
}

function sendPrivateMessage(to, from){
  //to is them
  //from is you
  $(".chat-form").submit(function(e){
    e.preventDefault();
    var msg = $("#private-message-" + to).val().trim();
    $("#private-message-" + to).val("");
    if(msg.length > 0 && msg.length <= 200){
      $("#private-messages-" + to).append(generateMessage(generateOwnMessage(msg), "YOU"));
      socket.emit("private message", {
        "to": to,
        "from": from,
        "message": msg
      });
    }
  });
}

function closeList(){
  $(".chat-form-container").css("position", "fixed");
    $(".list").animate({"left": "-100%"}, 500);
    setTimeout(function(){
      $(".all-users-container").css({"display": "none"});
      $(".chat-form-container").animate({"bottom": "0"}, 500);
    }, 500);
}

function connectionStatus(){
  if(!socket.connected){
    disconnectedMessage();
  }
  return socket.connected;
}

function generateMessage(msg, from){
  if(from == "YOU"){
    return $("<li class='align-left'>").html(msg);
  }else if(from == "OTHER"){
    return $("<li class='align-right'>").html(msg);
  }else if(from == "ADMIN"){
    return $("<li class='center'>").html('<span class="admin-message">'+ msg +'</span>');
  }
}

function generateStrangerMessage(name, msg){
  var messageStranger = $("<div class='message stranger'>");
  var strangerName = $("<span class='name'>");
  var message = $("<span>");
  var br = $("<br>");

  message.html(msg);
  strangerName.html(name);
  
  messageStranger.append(strangerName);
  messageStranger.append(br);
  messageStranger.append(message);

  return $("<li class='message'>").html(messageStranger);
  
  // return "<li class='message'><div class='message stranger'><span class='name'>" + name + " :</span><br/><span>" + msg + "</span></div></li>";
}

function generateImageMessage(data){
  var imgContainer = $('<div class="image">');
  var image = $("<img>");
  image.attr("src", data);
  imgContainer.append(image);
  return $("<li class='message'>").html(imgContainer);
}

function generateOwnMessage(msg){
  return "<li class='message'><div class='message you'><span>" + msg + "</span></div></li>";
}

function connectUser(to, from){
  socket.emit("request connection", {
    "from": from,
    "to": to
  });
}

function createMessageRequestModal(from, to, name){
  // to us you
  // from is them
  var container = document.createElement("div");
  container.classList.add("modal-container");
  var div = document.createElement("div");
  div.classList.add("message-request-box", "z-index-10000");
  var voidScr = document.createElement("div");
  voidScr.classList.add("modal-void");
  var content = document.createElement("p");
  content.classList.add("modal-heading", "center");
  var text = document.createTextNode(name + " Wants To Talk To You");
  var buttonsContainer = document.createElement("div");
  buttonsContainer.classList.add("buttons-container", "center");
  var acceptButton = document.createElement("button");
  acceptButton.classList.add("accept");
  acceptButton.innerHTML = "Accept";
  acceptButton.setAttribute("data-to", from);
  var rejectButton = document.createElement("button");
  rejectButton.classList.add("reject");
  rejectButton.innerHTML = "Reject";
  rejectButton.setAttribute("data-to", from);
  buttonsContainer.appendChild(acceptButton);
  buttonsContainer.appendChild(rejectButton);
  content.appendChild(text);
  container.appendChild(div);
  container.appendChild(voidScr);
  div.appendChild(content);
  div.appendChild(buttonsContainer);
  document.body.appendChild(container);
  $(".modal-container").fadeIn(500, function(){
    toggleOverflowBody();
  });
  $(".accept").on("click", function(){
    var to = $(this).data("to");
    socket.emit("accept", {
      "from": id,
      "to": to
    });
    destroyMessageRequestModal();
  });
  $(".reject").on("click", function(){
    var to = $(this).data("to");
    socket.emit("reject", {
      "from": id,
      "to": to
    });
    destroyMessageRequestModal();
  });

}

function destroyMessageRequestModal(){
  $(".modal-container").fadeOut(500, function(){
    $(".modal-container").remove();
  });
  toggleOverflowBody();
}

function createChatPopup(name, to, from){
  //to is them
  //from is you
  var container = document.createElement("div");
  container.setAttribute("id", "container-" + to);
  container.classList.add("chat-popup-container");
  var chatPopup = document.createElement("div");
  chatPopup.classList.add("chat-popup", "z-index-10000");
  var chatPopupVoid = document.createElement("div")
  chatPopupVoid.classList.add("chat-popup-void");
  var chatPopupHeader = document.createElement("div");
  chatPopupHeader.classList.add("chat-popup-header");
  var heading = document.createElement("div");
  heading.classList.add("heading");
  var para = document.createElement("p");
  para.classList.add("center");
  para.innerHTML = name;
  heading.appendChild(para);
  var close = document.createElement("div");
  close.classList.add("close");
  var div = document.createElement("div");
  div.classList.add("back");
  var backButton = document.createElement("button");
  backButton.innerHTML = '<i class="fa fa-chevron-left"></i>';
  div.appendChild(backButton);
  var closeButton = document.createElement("button");
  closeButton.innerHTML = "&#10006;";
  backButton.addEventListener("click", function(){
    $(".chat-popup-container").fadeOut(500);
    toggleOverflowBody();
    window.location.hash = '';
  });
  closeButton.addEventListener("click", function(){
    userDisconnect(to, from);
    toggleOverflowBody();
    window.location.hash = '';
  })
  var messagesContainer = document.createElement("div");
  messagesContainer.classList.add("messages");
  var privateContainer = document.createElement("div");
  privateContainer.classList.add("private-container");
  var list = document.createElement("ul");
  list.classList.add("private-messages");
  list.setAttribute("id", "private-messages-" + to);
  var chatPopupForm = document.createElement("div");
  chatPopupForm.classList.add("chat-popup-form");
  var form = document.createElement("form");
  form.classList.add("chat-form");
  form.setAttribute("id", "chat-form-" + to);
  var form2 = document.createElement("form");
  form2.classList.add("image-upload-form");
  form2.setAttribute("id", "image-upload-form-" + to);
  form2.encoding = "multipart/form-data";
  var inputFile = document.createElement("input");
  inputFile.setAttribute("type", "file");
  inputFile.setAttribute("id", "image-upload-" + to);
  inputFile.accept = "image/*";
  form2.appendChild(inputFile);
  var formContainer = document.createElement("div");
  formContainer.classList.add("form-container");
  var inputContainer = document.createElement("div");
  inputContainer.classList.add("input", "input-field");
  var inputContainer2 = document.createElement("div");
  inputContainer2.classList.add("input", "input-btn");
  var input = document.createElement("input");
  input.classList.add("private-message");
  input.autocomplete = "off";
  input.autofocus = "true";
  input.maxLength = "200";
  input.placeholder = "Enter Message"
  input.setAttribute("data-to", to);
  input.setAttribute("data-from", from);
  input.setAttribute("id", "private-message-" + to);
  var buttonsContainer1 = document.createElement("div");
  buttonsContainer1.classList.add("buttons");
  var buttonsContainer = document.createElement("div");
  buttonsContainer.classList.add("buttons");
  var input2 = document.createElement("div");
  input2.classList.add("input", "input-btn");
  var button = document.createElement("button");
  button.classList.add("send");
  button.setAttribute("id", "send-" + from);
  button.addEventListener("click", function(){
    sendPrivateMessage(to, from);
  });
  var imageButton = document.createElement("button");
  imageButton.type = "button";
  imageButton.classList.add("image-send");
  imageButton.addEventListener("click", function(){
    document.getElementById("image-upload-" + to).click();
  });
  inputFile.addEventListener("change", function(e){
    e.preventDefault();
    sendImage(to, from);
  });
  var imageIcon = document.createElement("i");
  imageIcon.classList.add("fa", "fa-camera");
  imageButton.appendChild(imageIcon);
  var icon = document.createElement("i");
  icon.classList.add("fa", "fa-paper-plane");
  button.appendChild(icon);
  input2.appendChild(button);
  inputContainer2.appendChild(imageButton);
  buttonsContainer1.appendChild(inputContainer2);
  buttonsContainer.appendChild(input2);
  inputContainer.appendChild(input);
  formContainer.appendChild(buttonsContainer1);
  formContainer.appendChild(inputContainer);
  formContainer.appendChild(buttonsContainer);
  form.appendChild(formContainer);
  chatPopupForm.appendChild(form2);
  chatPopupForm.appendChild(form);
  privateContainer.appendChild(list);
  messagesContainer.appendChild(privateContainer);
  close.appendChild(closeButton);
  container.appendChild(chatPopup);
  container.appendChild(chatPopupVoid);
  chatPopup.appendChild(chatPopupHeader);
  chatPopup.appendChild(messagesContainer);
  chatPopup.appendChild(chatPopupForm);
  chatPopupHeader.appendChild(div);
  chatPopupHeader.appendChild(heading);
  chatPopupHeader.appendChild(close);
  var chatPopupVoid = document.createElement("div");
  chatPopupVoid.classList.add("chat-popup-void");
  document.body.appendChild(container);
  $(".chat-popup-container").fadeIn(500, function(){
    toggleOverflowBody();
  });
}

function toggleOverflowBody(){
  $("body").toggleClass("overflow-hidden");
}

function removeUser(id){
  socket.emit("remove user", {
    "id": id
  });
}

function userDisconnect(to, from){
  socket.emit("private user disconnect", {
    "to": to,
    "from": from
  });
  $("#container-" + to).fadeOut(100, function(){
    $(this).remove();
  });
}

function isFileImage(file) {
  const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

  return file && acceptedImageTypes.includes(file['type'])
}

async function sendImage(to, from){
  const file = document.getElementById("image-upload-" + to).files[0];
  if(isFileImage(file)){
    const imageCompressionOptions = {
      maxSizeMB: .5,
      useWebWorker: true,
      maxWidthOrHeight: 480
    }
    const reader = new FileReader();
    let blobImg;
    reader.onloadend = function () {
      // convert image file to base64 string
      blobImg = reader.result;
      socket.emit("private image", {
        "data": blobImg,
        "to": to,
        "from": from
      });
      $("#private-messages-" + to).append(generateMessage(generateImageMessage(blobImg), "YOU"));
    }
  
    if (file) {
      try{
        if(['image/gif'].includes(file['type'])){
          reader.readAsDataURL(file);
        }else{
          const compressedFile = await imageCompression(file, imageCompressionOptions);
          reader.readAsDataURL(compressedFile);
        }
      }catch (error){
        console.log(error);
      }
    }
  }else{
    alert("File type is not image");
  }
}

function showAds(){
  var script = $('<script async>');
  script.attr('src', 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
  var insTag = '<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9713603748410820" data-ad-slot="2198888087" data-ad-format="auto" data-full-width-responsive="true"></ins>';
  var newScript = $('<script>');
  newScript.append('(adsbygoogle = window.adsbygoogle || []).push({});');
  $('body').append(script);
  $('body').append(insTag);
  $('body').append(newScript);
}

function hashChanged(){
  var hash = window.location.hash;
  if(hash == ''){
    $('.chat-popup-container').fadeOut(500, function(){
      toggleOverflowBody();
    });
  }
}

function disconnectedMessage(){
  // create variables
  var disconnectedContainer = document.createElement('div');
  var messageContainer = document.createElement('div');
  var message = document.createElement('p');
  var closeBtn = document.createElement('div');
  

  // add classes
  disconnectedContainer.classList.add('disconnected-container');
  messageContainer.classList.add('message-container');
  message.classList.add('error-message');

  // create dom
  message.append('You\'ve been disconnected');
  messageContainer.append(message);
  disconnectedContainer.append(messageContainer);

  //append to main dom
  document.body.append(disconnectedContainer);
  setTimeout(function(){
    messageContainer.classList.add('active');
  }, 200);
  setTimeout(function(){
    messageContainer.classList.remove('active');
  }, 4000);
  setTimeout(function(){
    $('.disconnected-container').remove();
  }, 4200);
}