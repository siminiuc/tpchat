const pathh = require("path");
const http = require("http");
const express = require("express");
const socketIO = require("socket.io");
const publicPath = pathh.join(__dirname, "/../public");
const port = process.env.PORT || 3000;
const crypto = require("crypto-js");

let app = express();
let server = http.createServer(app);
let io = socketIO(server, {
  pingInterval: 0,
  pingTimeout: 0
});

//tpchat data
var clients = [];
var messages = [];
var users = 0;

// pixel-art data
var drawings = [];

app.use(express.static(publicPath));
app.all('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
 });

var tpchatnsp = io.of('/tpchat');
var pixelArtNsp = io.of('/pixel-art');

tpchatnsp.on("connection", function(socket) {

  var user;

  socket.emit('new user', {
    from: "Admin",
    text: "Welcome To TP Chat"
  });

  socket.on("new user", function(e){
    var customSocketID = removeCharacters(socket.id);
    socket.emit('custom id', customSocketID);
    clients.push({
      name: e.name,
      socketID: socket.id,
      customID: customSocketID
    });
    users = clients.length;
    updateConnectedUsers();
    emitAllUsers();
    user = e.name;
    messages.push({
      type: "admin-message",
      message: user + " Joined"
    });
    socket.broadcast.emit("user connected",{
      name: user
    });
    if(e.secret != null && e.secret == "RK011020006969"){
      var hash = crypto.MD5(e.secret).toString();
      tpchatnsp.to(socket.id).emit("secret verified", {
        "secret": hash
      });
      emitAllUsers();
    }
  });

  socket.emit("request username");

  socket.on("username", function(e){
    user = e.name;
  });

  socket.on("newMessage", function(msg) {
    if(messages.length >= 500){
      messages.pop;
    }
    messages.push({
      type: "user-message",
      name: msg.name,
      message: msg.message
    });
    socket.broadcast.emit("newMessage", {
      name: msg.name,
      message: msg.message
    });
  });

  socket.on("disconnect", function() {

    var socketID = getCustomID(socket.id);
    var name = getName(socketID);
    tpchatnsp.emit("private user disconnect", {
      "name": name,
      "from": socketID
    });

    clients = clients.filter(function(obj){
      return obj.customID !== socketID;
    });

    messages.push({
      type: "admin-message",
      message: user + " Disconnected"
    });

    emitAllUsers();
    if(user != undefined){
      tpchatnsp.emit("user disconnected",{name: user, id: socketID});
    }
    users--;
    updateConnectedUsers();
  });

  socket.on("typing", function(e){
    if(e.typing == true){
      socket.broadcast.emit("is typing", {
        user: e.user,
        typing: e.typing
      });
    }else{
      socket.broadcast.emit("is typing", {
        user: e.user,
        typing: e.typing
      });
    }
  });

  socket.on("clear", function(){
    socket.broadcast.emit("cleared");
  });

  socket.on("request connection", function(e){
    var name = getName(e.from);
    var id = getRealID(e.to);
    socket.broadcast.to(id).emit("request connection", {
      "to": e.to,
      "from": e.from,
      "name": name
    });
  });

  socket.on("private message", function(e){
    var name = getName(e.from);
    var toId = getRealID(e.to);
    tpchatnsp.to(toId).emit("private message", {
      "to": e.to,
      "from": e.from,
      "message": e.message,
      "name": name
    });
  });

  socket.on("private image", function(e){
    var toId = getRealID(e.to);
    tpchatnsp.to(toId).emit("private image", e);
  });

  socket.on("request messages", function(){
    socket.emit("saved messages", {
      message: messages
    });
  });

  socket.on("accept", function(e){
    var name1 = getName(e.to);
    var name2 = getName(e.from);
    var fromId = getRealID(e.from);
    tpchatnsp.to(fromId).emit("message status", {
      "status": "success",
      "to": e.to,
      "from": e.from,
      "name": name1
    });
    var toId = getRealID(e.to);
    tpchatnsp.to(toId).emit("message status", {
      "status": "success",
      "to": e.from,
      "from": e.to,
      "name": name2
    });
  });

  socket.on("remove user", function(e){
    var tmpId = getRealID(e.id);
    tpchatnsp.sockets[tmpId].disconnect();
  });

  socket.on("private user disconnect", function(e){
    var clients = io.of('/tpchat').clients();
    var toId = getRealID(e.to);
    var name = getName(e.to);
    if(clients[e.to] != undefined){
      tpchatnsp.to(toId).emit("private user disconnect", {
        "name": name,
        "to": e.to,
        "from": e.from
      });
    }else{
      tpchatnsp.to(toId).emit("private user disconnect", {
        "name": name,
        "to": e.to,
        "from": e.from
      });
    }
  });

});

pixelArtNsp.on('connection', function(socket){
  
  var roomName;

  socket.on('new pixel user', function(e){
    roomName = e.group;
    socket.join(e.group);
  });

  socket.on('request drawing', function(){
    if(drawings.length > 0){
      var drawingInRoom = drawings.filter(obj => {
        return obj.roomName == roomName;
      });
      socket.emit('existing drawing', drawingInRoom);
    }else{
      socket.emit('existing drawing', null);
    }
  });

  socket.on('drawing', function(e){
    drawings.push({
      roomName: roomName,
      cords: e
    });
    socket.to(roomName).emit('drawing', e);
  });

});















function emitAllUsers(){
  tpchatnsp.emit("allUsers", clients);
}

function updateConnectedUsers(){
  tpchatnsp.emit("users", {
    numbers: users
  });
}

function getName(id){
  var arr = clients.find(o => o.customID == id);
  if(arr == undefined){
    arr = clients.find(o => o.socketID == id);
  }
  return arr.name;
}

function removeCharacters(str){
  str = str.replace('#', '-');
  str = str.replace('/', '');
  return str;
}

function getRealID(customID){
  var id = clients.find(o => o.customID == customID);
  return id.socketID;
}

function getCustomID(id){
  var id = clients.find(o => o.socketID == id);
  return id.customID;
}

server.listen(port, function() {
  console.log("listening to 3000");
});